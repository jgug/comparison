import xmltodict
import json
import bson
import timeit
import os
import matplotlib.pyplot as plt


__author__ = 'Pavel Vashkel'


""" Paths for input and output files """
PATH = ""
FILE_NAME_XML = "testfile.xml"
FILE_NAME_JSON = "testfile.json"
FILE_NAME_JSON_PP = "testfile_pp.json"
FILE_NAME_BSON = "testfile.bson"

""" Bar chart color """
BAR_COLOR = '#9999ff'


def parse_XML(data):
    """
    Parse XML data to dictionary.
    Use xmltodic (https://github.com/martinblech/xmltodict.git)
    :param data: data to parse
    :return: python dictionary
    """
    return xmltodict.parse(data)


def to_JSON(data):
    """
    Convert data to JSON format.
    :param data:  data to convert
    :return: JSON
    """
    return json.dumps(data)


def to_JSON_pretty_print(data):
    """
    Convert data to JSON pretty print formatted
    :param data:  data to convert
    :return: JSON wia pretty print
    """
    return json.dumps(data, sort_keys=True, indent=4)


def to_BSON(data):
    """
    Convert data to BSON
    Use PyMongo (https://api.mongodb.org/python/current/)
    :param data: data to convert
    :return: BSON
    """
    return bson.BSON.encode(data)


def plot_bac_chart(labels, sizes):
    """
    Plot 2D bar chart with file types on X and file size in bytes on Y
    :param labels: array with file type names
    :param sizes: array of file sizes
    """
    X = range(1, len(bar_chart_values) + 1)
    plt.title("XMl, JSON and BSON comparison")
    plt.ylabel("Size in byte")
    plt.xlabel("File types")
    plt.xticks(map(lambda x: x+0.5, X), labels)
    plt.bar(X, bar_chart_values, facecolor=BAR_COLOR, edgecolor='black', width=1.0)
    for x, y in zip(X, bar_chart_values):
        plt.text(x + 0.5, y + 0.05, '%.0f byte' % y, ha='center', va='bottom')
    plt.show()


def get_path(path, file):
    """
    Return path to file
    :param path: path to file
    :param file: file name
    :return: full path to file
    """
    return path + file


if __name__ == '__main__':
    start = timeit.default_timer()

    file = open(get_path(PATH, FILE_NAME_XML), 'r')
    data = parse_XML(file)
    file.close()

    start_separate = timeit.default_timer()
    file = open(get_path(PATH, FILE_NAME_JSON), 'w')
    file.write(to_JSON(data))
    file.close()
    print("JSON compressing time: " + str(timeit.default_timer() - start_separate))

    start_separate = timeit.default_timer()
    file = open(get_path(PATH, FILE_NAME_JSON_PP), 'w')
    file.write(to_JSON_pretty_print(data))
    file.close()
    print("JSON with PP compressing time: " + str(timeit.default_timer() - start_separate))

    start_separate = timeit.default_timer()
    file = open(get_path(PATH, FILE_NAME_BSON), 'w')
    file.write(to_BSON(data))
    file.close()
    print("BSON compressing time: " + str(timeit.default_timer() - start_separate))

    bar_chart_values = []
    bar_chart_values.append(os.path.getsize(get_path(PATH, FILE_NAME_XML)))
    bar_chart_values.append(os.path.getsize(get_path(PATH, FILE_NAME_JSON)))
    bar_chart_values.append(os.path.getsize(get_path(PATH, FILE_NAME_JSON_PP)))
    bar_chart_values.append(os.path.getsize(get_path(PATH, FILE_NAME_BSON)))

    labels = ("XML", "JSON", "JSON with PP", "BSON")

    print("Total execution time: " + str(timeit.default_timer() - start) + " seconds")

    plot_bac_chart(labels, bar_chart_values)
