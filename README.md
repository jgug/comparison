# Comparison XML, JSON and BSON formats

#### Shows XML, JSON and BSON file sizes comparison

#### Represent bar chart plot for:

* Initial XML file
* JSON
* Prettry printed JSON
* BSON

#### Plot example:

![Plot](http://i.imgur.com/tiqYVYg.png "Bar chart")